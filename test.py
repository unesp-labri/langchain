from langchain.document_loaders import AsyncChromiumLoader
from langchain.document_transformers import BeautifulSoupTransformer

def main():
    loader = AsyncChromiumLoader(["https://labriunesp.org/","https://labriunesp.org/projetos"])
    html = loader.load()
    
    bs_transformer = BeautifulSoupTransformer()
    docs_transformed = bs_transformer.transform_documents(html,tags_to_extract=["p"])

    for doc in docs_transformed:
        print(f"The URL of this page is: {doc.metadata} \n")
        print(f"The content of this page is: {doc.page_content} \n")
        

if __name__=='__main__':
    main()
